import envsub from 'envsub';
import { join } from 'path';

const YAML_CONFIG_FILENAME = './config.yaml';
const {
  PORT,
  RABBITMQ_USER,
  RABBITMQ_PASSWORD,
  RABBITMQ_HOST,
  RABBITMQ_QUEUE_NAME,
  MAIL_HOST,
  MAIL_USER,
  MAIL_PASSWORD,
  MAIL_FROM,
  MAIL_PORT,
  AWO_OTP_URL,
  AWO_OTP_USER,
  AWO_OTP_PASSWORD,
  // ELASTIC SEARCH
  ELASTICSEARCH_NODE,
  ELASTICSEARCH_USERNAME,
  ELASTICSEARCH_PASSWORD,
  DATABASE_HOST,
  DATABASE_PORT,
  DATABASE_USERNAME,
  DATABASE_PASSWORD,
  DATABASE_NAME,
  DATABASE_TYPE,
  AWO_OTP_SENDER,
} = process.env;

const templateFile = join(__dirname, YAML_CONFIG_FILENAME);
const outputFile = join(__dirname, YAML_CONFIG_FILENAME);

const options = {
  all: false,
  diff: false,
  envs: [
    // port
    { name: 'PORT', value: PORT },
    //rabbitmq config
    { name: 'RABBITMQ_USER', value: RABBITMQ_USER },
    { name: 'RABBITMQ_PASSWORD', value: RABBITMQ_PASSWORD },
    { name: 'RABBITMQ_HOST', value: RABBITMQ_HOST },
    { name: 'RABBITMQ_QUEUE_NAME', value: RABBITMQ_QUEUE_NAME },
    //elastic search config
    { name: 'ELASTICSEARCH_NODE', value: ELASTICSEARCH_NODE },
    { name: 'ELASTICSEARCH_USERNAME', value: ELASTICSEARCH_USERNAME },
    { name: 'ELASTICSEARCH_PASSWORD', value: ELASTICSEARCH_PASSWORD },
    // mysql
    { name: 'DATABASE_HOST', value: DATABASE_HOST },
    { name: 'DATABASE_PORT', value: DATABASE_PORT },
    { name: 'DATABASE_USERNAME', value: DATABASE_USERNAME },
    { name: 'DATABASE_PASSWORD', value: DATABASE_PASSWORD },
    { name: 'DATABASE_NAME', value: DATABASE_NAME },
    { name: 'DATABASE_TYPE', value: DATABASE_TYPE },
    //mailconfig
    { name: 'MAIL_HOST', value: MAIL_HOST },
    { name: 'MAIL_USER', value: MAIL_USER },
    { name: 'MAIL_PASSWORD', value: MAIL_PASSWORD },
    { name: 'MAIL_FROM', value: MAIL_FROM },
    { name: 'MAIL_PORT', value: MAIL_PORT },
    // awo
    { name: 'AWO_OTP_URL', value: AWO_OTP_URL },
    { name: 'AWO_OTP_USER', value: AWO_OTP_USER },
    { name: 'AWO_OTP_PASSWORD', value: AWO_OTP_PASSWORD },
    { name: 'AWO_OTP_SENDER', value: AWO_OTP_SENDER },
  ],
  envFiles: [join(__dirname, YAML_CONFIG_FILENAME)],
  protect: false,
  syntax: 'default',
  system: true,
};

// create (or overwrite) the output file
export const envObjStart = () =>
  envsub({ templateFile, outputFile, options })
    .then(() => {
      console.log('env-sub has loaded');
    })
    .catch((err: Error) => {
      console.error(err.message);
    });
