import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { AppModule } from './app.module';

import helmet from 'helmet';
import { envObjStart } from './config/envsub.config';
async function bootstrap() {
  await envObjStart();
  const app = await NestFactory.create(AppModule);
  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.GRPC,
    options: {
      url: 'localhost:5567', // URL gRPC
      package: 'accountquery',
      loader: { keepCase: true },
      maxSendMessageLength: 10 * 1024 * 1024, // 10 MB
      maxReceiveMessageLength: 10 * 1024 * 1024, // 10 MB
      protoPath: [join(__dirname, 'users/user.proto')],
    },
  });
  app.use(helmet());
  app.startAllMicroservices();
  // console.log(
  //   `🚀 User service running on port ${configService.get('http.port')}`,
  // );
}
bootstrap();
